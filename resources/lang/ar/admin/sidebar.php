<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sidebar Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'languages'              => 'لغات الموقع',
    'settings'               => 'الاعدادات',
    'shipping methods'       => 'وسائل التوصيل',
    'free shipping'          => 'توصيل مجاني',
    'local shipping'         => 'توصيل داخلي',
    'outer shipping'         => 'توصيل خارجي',
    'view all'               => 'عرض الكل',
    'create new category'    => 'اضافة قسم جديد',
    'create new subcategory' => 'اضافة قسم فرعي جديد',
    'brands'                 => 'الماركات',
    'add new brand'          => 'اضافة ماركة جديدة',
    'tags'                   => 'العلامات',
    'add new tag'            => 'اضافة علامة جديدة',

];
