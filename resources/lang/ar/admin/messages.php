<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'required id'          => 'ID مطلوب',
    'required name'        => 'الاسم مطلوب',
    'required email'       => 'البريد الالكتروني مطلوب',
    'required slug'        => 'اسم الرابط مطلوب',
    'required photo'       => 'الصورة مطلوب',
    'required parent_id'   => 'اسم القسم مطلوب',
    'required value'       => 'الاسم مطلوب',
    'email email'          => 'نمط البريد الالكتروني غير صحيح',
    'unique email'         => 'البريد الالكتروني موجود بالفعل',
    'unique slug'          => 'اسم الرابط موجود بالفعل',
    'id exists'            => 'ID غير موجود',
    'exists parent_id'     => 'القسم غير موجود',
    'numeric plain_value'  => 'يجب أن تكون قيمة التوصيل عدد',
    'password min'         => 'يجب ان تكون كلمة المرور اكبر من 8 حروف أو أرقام',
    'password confirm'     => 'كلمة المرور غير متوافقة',
    'mimes photo'          => 'يجب أن تكون صيغة الصورة : jpg, jpeg, png',
    'success'              => 'تم التحديث بنجاح',
    'error'                => 'فشل التحديث',
    'deleted successfully' => 'تم الحذف بنجاح',
    'created successfully' => 'تم الاضافة بنجاح',
    'category not found'   => 'هذا القسم غير موجود',
    'brand not found'      => 'هذه الماركة غير موجودة',
    'tag not found'        => 'هذه العلامة غير موجودة',

];
