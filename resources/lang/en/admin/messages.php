<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'required id'          => 'ID is required',
    'required name'        => 'Name is required',
    'required email'       => 'Email is required',
    'required slug'        => 'Slug is required',
    'required photo'       => 'Photo is required',
    'required parent_id'   => 'Category is required',
    'required value'       => 'Name is required',
    'email email'          => 'Email must be an email ex:"abc@123.example"',
    'unique email'         => 'This email has already been taken',
    'unique slug'          => 'This slug has already been taken',
    'id exists'            => 'ID not exists',
    'exists parent_id'     => 'The category is not exists',
    'numeric plain_value'  => 'Delivery Value must be a numeric',
    'mimes photo'          => 'The photo must be a file of type: jpg, jpeg, png',
    'password min'         => 'Password must be more than 8 letters or numbers',
    'password confirm'     => 'The password confirmation does not match',
    'success'              => 'Updated Successfully',
    'error'                => 'Updating failed',
    'deleted successfully' => 'Deleted Successfully',
    'created successfully' => 'Created Successfully',
    'category not found'   => 'This category is not found',
    'brand not found'      => 'This brand is not found',
    'tag not found'        => 'This tag is not found',

];
